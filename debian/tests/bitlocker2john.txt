$ bitlocker2john

Usage: bitlocker2john -i <Image of encrypted memory unit>

Options:

  -h		Show this help
  -i		Image path of encrypted memory unit encrypted with BitLocker
